FROM steamcmd/steamcmd:ubuntu
ARG UID=1000

RUN set -x \
 && groupadd -g ${UID} steam \
 && useradd -u ${UID} -g ${UID} steam

RUN mv /root /home/steam \
 && cp -r /etc/skel /root \
 && chown -R steam:steam /home/steam

WORKDIR /home/steam
ENV USER=steam
ENV HOME=/home/steam
ENV INSTALL_PATH=/mnt/game

USER steam

ENTRYPOINT /usr/bin/steamcmd +force_install_dir ${INSTALL_PATH} +login anonymous +app_update ${GAME_ID} validate +quit
